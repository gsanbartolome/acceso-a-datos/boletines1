<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Ciclistas $model */

$this->title = 'Update Ciclistas: ' . $model->dorsal;
$this->params['breadcrumbs'][] = ['label' => 'Ciclistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dorsal, 'url' => ['view', 'dorsal' => $model->dorsal]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ciclistas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
