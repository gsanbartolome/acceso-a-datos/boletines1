<?php

namespace app\controllers;

use app\models\Ciclistas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CiclistasController implements the CRUD actions for Ciclistas model.
 */
class CiclistasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Ciclistas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclistas::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'dorsal' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ciclistas model.
     * @param int $dorsal Dorsal
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($dorsal)
    {
        return $this->render('view', [
            'model' => $this->findModel($dorsal),
        ]);
    }

    /**
     * Creates a new Ciclistas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Ciclistas();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'dorsal' => $model->dorsal]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ciclistas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $dorsal Dorsal
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($dorsal)
    {
        $model = $this->findModel($dorsal);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'dorsal' => $model->dorsal]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ciclistas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $dorsal Dorsal
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($dorsal)
    {
        $this->findModel($dorsal)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ciclistas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $dorsal Dorsal
     * @return Ciclistas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($dorsal)
    {
        if (($model = Ciclistas::findOne(['dorsal' => $dorsal])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
